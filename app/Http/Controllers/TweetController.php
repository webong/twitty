<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tweet;
use Auth;

class TweetController extends Controller
{
    public function store(Request $request){

    	 $this->validate($request,[
          'body' => 'required|max:140|min:5',
        ]);

    	  Tweet::create([
    	  	'user_id' => Auth::id(),
    	'body'=> $request->body
    ]);
    return redirect('/home')->with('status', 'You just yelled out to the World!');
    }


}
